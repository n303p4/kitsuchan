![kitsuchan](logo.png)

[![MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://gitlab.com/n303p4/kitsuchan/blob/master/LICENSE.txt)
[![Python](https://img.shields.io/badge/Python-3.6-brightgreen.svg)](https://python.org/)

**kitsuchan** is a small, modular Discord bot framework that extends `discord.py`,
with a focus on readable, portable, and easy to maintain code.

You may be looking for the bot Kitsuchan, which is can be found [here](https://gitlab.com/n303p4/kitsuchan-bot).

# How to install

```bash
python3 -m pip install --user -r requirements.txt
python3 setup.py install --user
```

You may have to change the above commands slightly, depending on your operating system and the
location of your Python installation.

