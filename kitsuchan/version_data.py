"""Version information about Kitsuchan"""

# pylint: disable=invalid-name

description = ("A light bot framework that extends discord.py.")
url = "https://gitlab.com/n303p4/kitsuchan"
version_info = (2, 8, 4, "Kinu")
version_number = "{0}.{1}.{2}".format(*version_info[:-1])
version = '{0}.{1}.{2} "{3}"'.format(*version_info)
