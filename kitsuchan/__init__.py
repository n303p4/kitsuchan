"""A light bot framework that extends discord.py."""

from . import core
from . import exceptions
from . import helpers
from .version_data import description, url, version_info, version_number, version
