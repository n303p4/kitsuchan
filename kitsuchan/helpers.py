"""Helper functions for Discord bots."""

# pylint: disable=invalid-name

import asyncio

import discord
from discord.ext import commands

memberconverter = commands.MemberConverter()
roleconverter = commands.RoleConverter()


async def yes_no(ctx: commands.Context,
                 message: str = "Are you sure? Type **yes** within 10 seconds to confirm. o.o"):
    """Prompt a user to answer a yes-no question, with a timeout of 10 seconds.

    * `ctx` - The context in which the question is being asked.
    * `message` - Optional messsage to display.
    """
    await ctx.send(message)
    try:
        message = await ctx.bot.wait_for(
            "message", timeout=10,
            check=lambda msg: msg.author == ctx.message.author and msg.channel == ctx.channel
        )
    except asyncio.TimeoutError:
        raise commands.UserInputError("Timed out waiting.")

    if message.clean_content.lower() not in ["yes", "y"]:
        return False

    return True


async def input_text(ctx: commands.Context,
                     message: str = "Please enter some text within 10 seconds.",
                     *, timeout: int = 10):
    """Prompt a user to input some text.

    * `ctx` - The context in which the question is being asked.
    * `message` - Optional messsage to display.
    * `timeout` - Timeout, in seconds, before automatically failing.
    """
    await ctx.send(message)
    try:
        message = await ctx.bot.wait_for(
            "message", timeout=timeout,
            check=lambda msg: msg.author == ctx.message.author and msg.channel == ctx.channel
        )
    except asyncio.TimeoutError:
        raise commands.UserInputError("Timed out waiting.")

    return message.clean_content


async def input_number(ctx: commands.Context,
                       message: str = "Please enter a number within 10 seconds.",
                       *, timeout: int = 10, min_value: int = None, max_value: int = None):
    """Prompt a user to input a number.

    * `ctx` - The context in which the question is being asked.
    * `message` - Optional messsage to display.
    * `timeout` - Timeout, in seconds, before automatically failing.
    * `min_value` - Minimum accepted value for the input.
    * `max_value` - Maximum accepted value for the input.
    """
    await ctx.send(message)

    def check(message):
        """A checking function dynamically to verify input."""
        if message.author != ctx.message.author or not message.clean_content.isdecimal() \
        or message.channel != ctx.channel:
            return False

        number = int(message.clean_content)

        if (min_value and number < min_value) or (max_value and number > max_value):
            return False

        return True

    try:
        message = await ctx.bot.wait_for("message", timeout=timeout, check=check)

    except asyncio.TimeoutError:
        raise commands.UserInputError("Timed out waiting.")

    return int(message.clean_content)


async def member_by_substring(ctx: commands.Context, substring: str):
    """Search for a member using substring matching. Checks both usernames and nicknames.

    * `ctx` - The context under which to search for members.
    * `substring` - A substring to match.
    """
    try:
        return await memberconverter.convert(ctx, substring)
    except commands.CommandError:
        pass

    substring = substring.lower()

    for member in ctx.guild.members:
        if substring in member.name.lower() or substring in member.display_name.lower():
            return member

    raise commands.CommandError("No user with that substring was found.")


async def role_by_substring(ctx: commands.Context, substring: str):
    """Search for a role using substring matching.

    * `ctx` - The context under which to search for roles.
    * `substring` - A substring to match.
    """
    try:
        return await roleconverter.convert(ctx, substring)
    except commands.CommandError:
        pass

    substring = substring.lower()

    for role in ctx.guild.roles:
        if substring in role.name.lower():
            return role

    raise commands.CommandError("No role with that substring was found.")


async def get_last_image(channel):
    """Retrieve the last image posted in a channel. Scans embeds and attachments.

    Returns an image URL as an `str`, or failing that, raises `discord.CommandError`.

    * `channel` - The channel to search for images in.
    """
    image_url = None
    async for message in channel.history():
        if message.embeds:
            for embed in reversed(message.embeds):
                if embed.type == "image":
                    image_url = embed.url
                    break
                elif (embed.type == "rich" and
                      embed.image is not discord.Embed.Empty):
                    image_url = embed.image.url
                    break
        elif message.attachments:
            for attachment in message.attachments:
                if attachment.height:
                    image_url = attachment.url
                    break
        if image_url:
            break

    if not image_url:
        raise commands.CommandError("No images found in recent chat history.")
    return image_url
