"""Install Kitsuchan"""

from setuptools import setup

setup(name="kitsuchan",
      version="2.8.4",
      description="A Discord bot framework that builds upon discord.py",
      license="MIT",
      packages=["kitsuchan"],
      zip_safe=False)
